import { createApp } from 'vue'
import App from './App.vue'

import firebase from 'firebase/compat/app';
import { getDatabase } from "firebase/database";
import { getFirestore } from "firebase/firestore";

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
    apiKey: "AIzaSyBWSGNM4WkPGOOpAevU2_i5SiZrDQhNJOw",
    authDomain: "cloud-lab-5-2a32c.firebaseapp.com",
    databaseURL:"https://cloud-lab-5-2a32c-default-rtdb.europe-west1.firebasedatabase.app/",
    projectId: "cloud-lab-5-2a32c",
    storageBucket: "cloud-lab-5-2a32c.appspot.com",
    messagingSenderId: "344067866952",
    appId: "1:344067866952:web:9480dfcf47e11b3982737d",
    measurementId: "G-SDKM6NN2FW"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);
export const database = getDatabase(firebaseApp);
export const storedb = getFirestore(firebaseApp);

createApp(App).mount('#app')


