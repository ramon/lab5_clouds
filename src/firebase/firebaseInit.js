// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/database";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBWSGNM4WkPGOOpAevU2_i5SiZrDQhNJOw",
  authDomain: "cloud-lab-5-2a32c.firebaseapp.com",
  projectId: "cloud-lab-5-2a32c",
  storageBucket: "cloud-lab-5-2a32c.appspot.com",
  messagingSenderId: "344067866952",
  appId: "1:344067866952:web:9480dfcf47e11b3982737d",
  measurementId: "G-SDKM6NN2FW"
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

export var firestore = firebaseApp.firestore();
export var database =  firebaseApp.database();